#!/bin/env python3

import paho.mqtt.client as mqtt
import socket
import json
import time

MQTT_SERVER  = "mqtt"
MQTT_PORT    = 1883
MQTT_TIMEOUT = 60
MQTT_TOPIC   = "co2_sensor/telemetry"

CARBON_SERVER = "graphite"
CARBON_PORT   = 2003

def on_connect(client, userdata, flags, rc):
    print("Connected with result code", str(rc))
    client.subscribe(MQTT_TOPIC)

def on_message(client, userdata, msg):
    print(msg.topic, str(msg.payload))
    payload = json.loads(msg.payload.decode("ascii"))
    print("    ", payload)
    timestamp = int(time.time())
    message = f"""co2_sensor.co2 {payload['co2']} {timestamp}
co2_sensor.temperature {payload['temperature']} {timestamp}
co2_sensor.rh {payload['rh']} {timestamp}
"""
    print(f'sending message:\n{message}')
    sock = socket.socket()
    sock.connect((CARBON_SERVER, CARBON_PORT))
    sock.sendall(bytes(message, encoding="ascii"))
    sock.close()
    
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

print("MQTT forwarder started")
client.connect(MQTT_SERVER, MQTT_PORT, MQTT_TIMEOUT)
client.loop_forever()
