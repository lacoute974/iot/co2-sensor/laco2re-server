CO2 sensor visualization server
===============================

This server is part of the CO2 sensor project developed as an exercise for the
"Introduction to Yocto" presentation in https://gitlab.com/lacoute974/iot/co2-sensor.

It requires an Ubuntu 20.04 server.
It shall be executed from a deployement host (for instance the developer's PC).

Security Disclaimer
-------------------

This server deployment is only indented to serve as a demonstration of the
Yocto CO2 sensor project.

IT HAS NO SECURITY FEATURE whatsoever.

Only use this server to test the Yocto CO2 Sensor project and for a brief
period of time.

All traffic is in plaintext, there is no authentication, and the web server
is HTTP only.

You've been warned. THIS SERVER IS HIGHLY INSECURE.

Deployment
----------

To deploy the server:

- Install Ubuntu 20.04 on a server.
- Install docker and docker-compose on the server.
- Install docker-compose on the deployment host.
- Give SSH access to the user from the deployment host (preferably key-based access,
  instead of password-based)
- Clone this project on the deployment host.
- From the project's root directory:

```bash
$ DOCKER_HOST=target.server.host.name docker-compose up -d
```

- Visit the server's web site http://target.server.host.name
- By default, the grafana credentials are admin/admin. Change the password.
- In Configuration/Data Sources, add a graphite data source with the following
  parameters:
    - URL: http://graphite
    - leave all other options by default
- In the '+' menu, click "Import", then "Upload JSON file"
- Upload the file `grafana/dashboard.json`

The SCD30 MQTT bridge service in the Yocto CO2 sensor project points
to a sample server. You'll need to replace the server's URL with your own
server's.

Server Architecture
===================

```
                 __________
                |          |
                |   WEB    |
                |  Client  |
                ------------
               /......... /
              /......... /
             /__________/
                   ^
                   | HTTP
      . . . . . . .|. . . . . . . .
                   |
            +-------------+
            |   GRAFANA   |
            +-------------+
                  ^
                  | graphite query
                  |
            +-------------+
            |  Graphite   |
            |  database   |
            +-------------+
                  ^
                  | plaintext port 2003
                  |
        +--------------------+
        |   MQTT forwarder   |
        +--------------------+
                  ^
                  | MQTT
                  |
         +----------------+
         |  MQTT broker   |
         +----------------+
                  ^
                  |
         . . . . .|. . . . . .
                  | MQTT
                  |
       +----------------------+
       | CO2 sensor appliance |
       +----------------------+
```